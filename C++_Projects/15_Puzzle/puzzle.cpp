#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <iomanip>
#include <queue>
#include <stack>
#include <fstream>
using namespace std;

//globals
const int maxDepth = 80;
const int sz = 16;
int moves[4] = {0, 0, 0, 0};

struct node
{
    int state[sz];
    int hValue;
    int gValue;
    string move;
    node *backPtr;
};

int getHvalue(node);
void printPuzzle(int[]);
void getMoves(node);
void generateChildren(node);
void swap(int[], int, int);
void printNodeContents(node);
bool checkSolution(node);
node findMinH();

node initialState;

queue<node> openQ;      //tracks open nodes
queue<node> closedQ;    //tracks closed nodes

ofstream outF;

int main()
{
    outF.open("path.txt");
    int minH;
    int s[sz] = {0, 6, 2, 4, 1, 10, 3, 7, 5, 9, 14, 8, 13, 15, 11, 12};
    int sz = sizeof(s);

    memcpy(initialState.state, s, 64);  //set initialState.state as s[]
    initialState.gValue = 0;
    initialState.hValue = getHvalue(initialState);
    initialState.move = "START";
    initialState.backPtr = NULL;
    openQ.push(initialState);
    printNodeContents(initialState);
    
    node n;
    n = findMinH();
    
    while (!checkSolution(n))
    {
        if (n.gValue >= maxDepth)
        {
            closedQ.push(n);
            n = findMinH();
        }
        generateChildren(n);
        n = findMinH();
        printNodeContents(n);
    }

    outF << "DONE" << endl;
    outF << endl;
    outF << "Number of nodes generated: " << openQ.size() + closedQ.size() << endl;
    outF << "Length of solution path: " << n.gValue << endl;

    cout << "DONE" << endl;
    cout << endl;
    cout << "Number of nodes generated: " << openQ.size() + closedQ.size() << endl;
    cout << "Length of solution path: " << n.gValue << endl;
    

    return 0;
}

//this function checks the position of the numbers in the puzzle
bool checkSolution(node puzzle)
{
    for (int i=0; i<sz-1; i++)
    {
        if (puzzle.state[i] != i+1)
            return false;
    }

    return true;
}

//this function prints the contents of each node
void printNodeContents(node puzzle)
{
    outF << setw(12) << right << puzzle.move << endl;
    cout << setw(12) << right << puzzle.move << endl;
    printPuzzle(puzzle.state);
    outF << endl;
    cout << endl;
}

//determines the minimum h value from the pool of generated children on the open queue
node findMinH()
{
    int min;
    int sizeQ = openQ.size();
    node tempNode = openQ.front();      //gets front node of open queue
    node swap;

    if (sizeQ == 1)
    {
        openQ.pop();
        return tempNode;
    }
    else
    {
        openQ.pop();
        node openQarr[sizeQ-1];
        for (int i=0; i<sizeQ-1; i++)
        {
            openQarr[i] = openQ.front();
            openQ.pop();
        }

        min = tempNode.hValue;

        for (int i=0; i<sizeQ-1; i++)
        {
            if (openQarr[i].hValue < min)
            {
                min = openQarr[i].hValue;
                swap = openQarr[i];
                openQarr[i] = tempNode;
                tempNode = swap;
            }
        }

        for (int i=0; i<sizeQ-1; i++)
            openQ.push(openQarr[i]);

        return tempNode;
    }
}

//blank tile movement = array position swapping
void swap(int t[], int oldPos, int newPos)
{
    int tmp;
    tmp = t[oldPos];
    t[oldPos] = t[newPos];
    t[newPos] = tmp;
}

//This function generates successors.
//First, it finds the position of the blank in the puzzle and determines which moves are valid
//The valid moves are stored on the array moves[], with each position representing a movement
//moves[0] = up, moves[1] = down, etc.
//This function avoids duplicate/redundant moves by checking the parent's move
//Successors are generated based on the moves array and placed in the open queue
void generateChildren(node parent)
{
    //find possible moves for blank
    //moves[0] = up
    //moves[1] = down
    //moves[2] = left
    //moves[3] = right

    closedQ.push(parent);
    int blankPos;

    //iterate parent.state[] to find where blank is
    for (int i=0; i<sz; i++)
    {
        if (parent.state[i] == 0)
        {
            if (i == 5 || i == 6 || i == 9 || i == 10)
            {
                moves[0] = moves[0] + 1;
                moves[1] = moves[1] + 1;
                moves[2] = moves[2] + 1;
                moves[3] = moves[3] + 1;
            }
            
            else if (i % 4 == 0)
            {
                moves[3] = moves[3] + 1;
                if (i != 0 && i != 12)
                {
                    moves[0] = moves[0] + 1;
                    moves[1] = moves[1] + 1;
                }
                else if (i == 0)
                    moves[1] = moves[1] + 1;
                    
                else
                    moves[0] = moves[0] + 1;
            }

            else if (i == 3 || i == 7 || i== 11 || i == 15)
            {
                moves[2] = moves[2] + 1;
                if (i != 3 && i != 15)
                {
                    moves[0] = moves[0] + 1;
                    moves[1] = moves[1] + 1;
                }
                else if (i == 3)
                    moves[1] = moves[1] + 1;
                else
                    moves[0] = moves[0] + 1;
            }

            else
            {
                moves[2] = moves[2] + 1;
                moves[3] = moves[3] + 1;
                if (i == 1 || i == 2)
                    moves[1] = moves[1] + 1;
                else
                    moves[0] = moves[0] + 1;
            }
            blankPos = i;
            break;
        } //end if statement
    } //end for loop

    //populate temp[] with parent.state[]
    int temp[sz];
    for (int i=0; i<sz; i++)
        temp[i] = parent.state[i];
    

    //avoids duplicate nodes
    if (parent.move == "UP")
        moves[1] = moves[1] - 1;

    if (parent.move == "DOWN")
        moves[0] = moves[0] - 1;

    if (parent.move == "LEFT")
        moves[3] = moves[3] - 1;

    if (parent.move == "RIGHT")
        moves[2] = moves[2] - 1;

    if (moves[0] == 1)
    {
        swap(temp, blankPos, blankPos-4);
        node a;
        memcpy(a.state, temp, 64);
        a.gValue = parent.gValue + 1;
        a.hValue = getHvalue(a);
        a.backPtr = &parent;
        a.move = "UP";
        openQ.push(a);
        swap(temp, blankPos, blankPos-4);
    }

    if (moves[1] == 1)
    {
        swap(temp, blankPos, blankPos+4);
        node b;
        memcpy(b.state, temp, 64);
        b.gValue = parent.gValue + 1;
        b.hValue = getHvalue(b);
        b.backPtr = &parent;
        b.move = "DOWN";
        openQ.push(b);
        swap(temp, blankPos, blankPos+4);
    }

    if (moves[2] == 1)
    {
        swap(temp, blankPos, blankPos-1);
        node c;
        memcpy(c.state, temp, 64);
        c.gValue = parent.gValue + 1;
        c.hValue = getHvalue(c);
        c.backPtr = &parent;
        c.move = "LEFT";
        openQ.push(c);
        swap(temp, blankPos, blankPos-1);
    }

    if (moves[3] == 1)
    {
        swap(temp, blankPos, blankPos+1);
        node d;
        memcpy(d.state, temp, 64);
        d.gValue = parent.gValue + 1;
        d.hValue = getHvalue(d);
        d.backPtr = &parent;
        d.move = "RIGHT";
        openQ.push(d);
        swap(temp, blankPos, blankPos+1);
    }
    for (int i=0; i<4; i++)
        moves[i] = 0;
}

//Heuristic function that counts how many numbers are out of place and adds the depth of the node.
int getHvalue(node puzzle)
{
    int count = 0;
    for (int i=0; i<sz-1; i++)
    {
        if (puzzle.state[i] != i+1)
            count++;
    }
    return count + puzzle.gValue;
}

void printPuzzle(int puzzle[])
{
    int count = 0;
    int w = 4;
    outF << endl;
    cout << endl;
    for (int i=0; i<sz; i++)
    {
        if (puzzle[i] == 0){
            outF << right << setw(w) << "[]";
            cout << right << setw(w) << "[]";
        } else {
            outF << right << setw(w) << puzzle[i];
            cout << right << setw(w) << puzzle[i];
        }
        count++;
        if (count % 4 == 0) {
            outF << endl;
            cout << endl;
        }
    }
}
