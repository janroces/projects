package com.example.myapplication;

import android.app.Application;
import android.content.Intent;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

// When the entire application is run, this is the first class to be executed. This class
// will perform a check to see if the users were still logged in before they closed and
// re-opened the app.
// - If the users were still logged in when they reopen the app, it'll pick up where they left off.
// - Else, the users will be shown the Welcoming Screen (i.e. WelcomeScreenActivity)
public class Home extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseAuth myFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser munchiUser = myFirebaseAuth.getCurrentUser();

        if (munchiUser != null && munchiUser.isEmailVerified()) {
            Intent myIntent = new Intent(Home.this, HomePage.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myIntent);
        }
    }
}
