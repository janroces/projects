package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Timer;
import java.util.TimerTask;


// -----
// Christmas Edition Splash Page
public class SplashStartActivity extends AppCompatActivity {

    FirebaseAuth myFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser munchiUser = myFirebaseAuth.getCurrentUser();

    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        if (munchiUser != null && munchiUser.isEmailVerified()) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash_start);

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashStartActivity.this, HomePage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }, 2000);

        }
        else {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash_start);

            // For transition
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashStartActivity.this, WelcomeScreenActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 2000);
        }

    }
}
