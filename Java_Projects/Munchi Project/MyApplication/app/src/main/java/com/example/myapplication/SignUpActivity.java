package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {
    private EditText email, password1, password2;
    private Button backButton, signUpButton;

    private FirebaseAuth myFirebaseAuth;
    private DatabaseReference munchiDBref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        email = (EditText) findViewById(R.id.emailSignUp_ET);
        password1 = (EditText) findViewById(R.id.passwordSignUp1_ET);
        password2 = (EditText) findViewById(R.id.passwordSignUp2_ET);
        signUpButton = (Button) findViewById(R.id.registerBtn_signUp);

        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Sign Up ENTER
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyInfo();
            }
        });
    }

    private void verifyInfo() {
        if (email.getText().toString().trim().equals("") || password1.getText().toString().equals("") || password2.getText().toString().equals(""))
            Toast.makeText(getBaseContext(), "Please fill in the email and both password entries.", Toast.LENGTH_LONG).show();
        else if (!(password1.getText().toString().equals(password2.getText().toString()))) {
            Toast.makeText(getBaseContext(), "The password entries don't match. Please try again.", Toast.LENGTH_LONG).show();
            password1.setText("");
            password2.setText("");
        }
        else
            insertData();
    }

    private void insertData() {
        // addOnCompleteListener helps us determine whether the operation we called it on was successful or not
        myFirebaseAuth.createUserWithEmailAndPassword(email.getText().toString(), password1.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    myFirebaseAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getBaseContext(), "A verification email has been sent to your email address", Toast.LENGTH_LONG).show();
                                signUpPage4();
                            }
                            else
                                Toast.makeText(getBaseContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else
                    Toast.makeText(getBaseContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void signUpPage4() {
        Intent intent = new Intent(this, EnterUserMetricsActivity.class);
        startActivity(intent);
    }
}
