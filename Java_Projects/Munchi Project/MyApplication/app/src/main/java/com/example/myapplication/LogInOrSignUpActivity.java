package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LogInOrSignUpActivity extends AppCompatActivity {
    private Button logIn, signUp, backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_or_sign_up);

        logIn = (Button) findViewById(R.id.skip_logInBTN);
        signUp = (Button) findViewById(R.id.skip_signUpBTN);
        backButton = (Button) findViewById(R.id.skip_backBtn);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logInPage();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpPage();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void logInPage() {
        Intent myIntent = new Intent(this, LogInActivity.class);
        startActivity(myIntent);
    }

    private void signUpPage() {
        Intent myIntent = new Intent(this, SignUpActivity.class);
        startActivity(myIntent);
    }
}
