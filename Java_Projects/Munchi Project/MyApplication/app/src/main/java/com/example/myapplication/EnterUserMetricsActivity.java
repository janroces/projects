package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class EnterUserMetricsActivity extends AppCompatActivity {
    private Button nextButton;
    public static EditText age, heightFeet, heightInches, weight;   // These fields are public because they get called
    public static RadioButton male, female;                         // in ActivityLevel.java, and their values are
    public static RadioGroup radioGroup;                            // stored in a User object.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user_metrics);

        nextButton = (Button) findViewById(R.id.next_button);
        age = (EditText) findViewById(R.id.age_editText);
        heightFeet = (EditText) findViewById(R.id.height_feet_editText);
        heightInches = (EditText) findViewById(R.id.height_inches_editText);
        weight = (EditText) findViewById(R.id.userWeight_in_pounds_edittext);
        male = (RadioButton) findViewById(R.id.male_radioButton);
        female = (RadioButton) findViewById(R.id.female_radioButton);
        radioGroup = (RadioGroup) findViewById(R.id.gender_radio_group);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyInfo();
            }
        });
    }

    private void verifyInfo() {
        if (age.getText().toString().equals("") || Integer.parseInt(age.getText().toString()) < 12) {
            Toast.makeText(getBaseContext(), "Invalid age. Please try again.", Toast.LENGTH_LONG).show();
            age.setText("");
        }
        else if (!male.isChecked() && !female.isChecked())
            Toast.makeText(getBaseContext(), "Please select a gender.", Toast.LENGTH_LONG).show();
        else if (heightFeet.getText().toString().equals("") || Integer.parseInt(heightFeet.getText().toString()) < 3 || Integer.parseInt(heightFeet.getText().toString()) > 9) {
            Toast.makeText(getBaseContext(), "Invalid height. Please try again.", Toast.LENGTH_LONG).show();
            heightFeet.setText("");
        }
        else if (heightInches.getText().toString().equals("") || Integer.parseInt(heightInches.getText().toString()) < 0 || Integer.parseInt(heightInches.getText().toString()) > 11) {
            Toast.makeText(getBaseContext(), "Invalid height. Please try again.", Toast.LENGTH_LONG).show();
            heightInches.setText("");
        }
        else if (weight.getText().toString().equals("") || Integer.parseInt(weight.getText().toString()) < 20) {
            Toast.makeText(getBaseContext(), "Invalid weight. Please try again.", Toast.LENGTH_LONG).show();
            weight.setText("");
        }
        else
            signUpPage3();
    }

    private void signUpPage3() {
        Intent myIntent = new Intent(this, ActivityLevel.class);
        startActivity(myIntent);
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(Intent.ACTION_MAIN);
        myIntent.addCategory(Intent.CATEGORY_HOME);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
    }
}
