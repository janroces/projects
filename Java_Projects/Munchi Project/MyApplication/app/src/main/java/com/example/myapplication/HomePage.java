package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.txusballesteros.widgets.FitChart;

import com.txusballesteros.widgets.FitChart;

import java.util.List;


public class HomePage extends AppCompatActivity {

    // -----
    // Pete's Additions
    // The Dock Bar Buttons private variables
    private ImageButton recipeButton; // Declaration of recipe button
    private ImageButton profileButton;

    // Michelle & Cheyenne's
    private FirebaseAuth myFirebaseAuth;
    private FirebaseUser munchiUser;
    private DatabaseReference munchiDBref;

    public static TextView userCalories;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        recipeButton = (ImageButton) findViewById(R.id.recipe_button); // For Recipe Page
        profileButton = (ImageButton) findViewById(R.id.profile_button);
        userCalories = (TextView) findViewById(R.id.consumed_value);

        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiUser = myFirebaseAuth.getCurrentUser();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        recipeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                recipePage();
            }
        });
        profileButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                profilePage();
            }
        });


        // this calls the calorie calculator
        myCalculator();
    }



    // -----
    // Pete's Additions
    // The Dock Bar Transition Functions

    // Recipe Page Transition
    private void recipePage() {
        Intent intent = new Intent(this, EnterIngredientsActivity.class);
        startActivity(intent);
    }

    private void profilePage() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    private void myCalculator() {

        Query userQuery = munchiDBref.orderByChild("email").equalTo(munchiUser.getEmail());
        userQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    Users newUser = singleSnapshot.getValue(Users.class);

                    // fetching user's information
                    String gender = newUser.getGender();
                    String userStatus = newUser.getActivityLevel();
                    double weightInPounds = newUser.getWeight();
                    double age = newUser.getAge();
                    double heightFt = newUser.getHeightInFeet();
                    double heightIn = newUser.getHeightInInches();

                    // hard coding consumed calories for now
                    //double consumed = 500;
                    double consumed = newUser.getCalories();  //Jan's addition to get calorie total;
                    double bmr = 0.0;
                    double sedentaryCalories = 0.0;
                    double lightlyActiveCalories = 0.0;
                    double moderatelyActiveCalories = 0.0;
                    double veryActiveCalories = 0.0;
                    double superActiveCalories = 0.0;
                    double sedentaryCaloriesRemaining = 0.0;
                    double lightlyActiveCaloriesRemaining = 0.0;
                    double moderatelyActiveCaloriesRemaining = 0.0;
                    double veryActiveCaloriesRemaining = 0.0;
                    double superActiveCaloriesRemaining = 0.0;
                    int sedentaryPercentage = 0;
                    int lightlyActivePercentage = 0;
                    int moderatelyActivePercentage = 0;
                    int veryActivePercentage = 0;
                    int superActivePercentage = 0;

                    // converting weight from pounds to kilograms for st. jeor
                    double weight = weightInPounds * 0.45359237;

                    // converting height from feet and height to centimeters for st. jeor
                    double height;
                    heightFt = heightFt * 30.48;
                    heightIn = heightIn * 2.54;
                    height = heightFt + heightIn;

                    // calculate bmr based on user's gender
                    if (gender.equals("Male"))
                        bmr = 10 * weight + 6.25 * height - 5 * age + 5;
                    else if (gender.equals("Female"))
                        bmr = 10 * weight + 6.25 * height - 5 * age - 161;

                    // calculate user's needed calories
                    sedentaryCalories = bmr * 1.2;
                    lightlyActiveCalories = bmr * 1.375;
                    moderatelyActiveCalories = bmr * 1.55;
                    veryActiveCalories = bmr * 1.725;
                    superActiveCalories = bmr * 1.9;

                    // calculate user's remaining calories
                    sedentaryCaloriesRemaining = sedentaryCalories - consumed;
                    lightlyActiveCaloriesRemaining = lightlyActiveCalories - consumed;
                    moderatelyActiveCaloriesRemaining = moderatelyActiveCalories - consumed;
                    veryActiveCaloriesRemaining = veryActiveCalories - consumed;
                    superActiveCaloriesRemaining = superActiveCalories - consumed;

                    // used for chart
                    sedentaryPercentage = (int)Math.round((consumed / sedentaryCalories) * 100);
                    lightlyActivePercentage = (int)Math.round((consumed / lightlyActiveCalories) * 100);
                    moderatelyActivePercentage = (int)Math.round((consumed / moderatelyActiveCalories) * 100);
                    veryActivePercentage = (int)Math.round((consumed / veryActiveCalories) * 100);
                    superActivePercentage = (int)Math.round((consumed / superActiveCalories) * 100);

                    // Now the following outputs to the screen
                    // based on user's activity level
                    if (userStatus.equals("Sedentary")) {
                        TextView printUserStatus = (TextView) findViewById(R.id.activity_type);
                        printUserStatus.setText(userStatus + " Target");

                        final FitChart fitChart = (FitChart)findViewById(R.id.fitChart);
                        fitChart.setValue(sedentaryPercentage);

                        TextView percentText = (TextView) findViewById(R.id.percent_indicator);
                        percentText.setText(sedentaryPercentage + "%");

                        TextView conNeedText = (TextView) findViewById(R.id.consumed_over_needed);
                        conNeedText.setText((int)Math.round(consumed) + "cal/" + (int)Math.round(sedentaryCalories) + "cal");

                        TextView consumedText = (TextView) findViewById(R.id.consumed_value);
                        consumedText.setText((int)Math.round(newUser.getCalories()) + " cal");

                        TextView neededText = (TextView) findViewById(R.id.needed_value);
                        neededText.setText((int)Math.round(sedentaryCalories) + " cal");

                        TextView leftoverText = (TextView) findViewById(R.id.leftover_value);
                        leftoverText.setText((int)Math.round(sedentaryCaloriesRemaining) + " cal");
                    }
                    else if (userStatus.equals("Lightly Active")) {
                        TextView printUserStatus = (TextView) findViewById(R.id.activity_type);
                        printUserStatus.setText(userStatus + " Target");

                        final FitChart fitChart = (FitChart)findViewById(R.id.fitChart);
                        fitChart.setValue(lightlyActivePercentage);

                        TextView percentText = (TextView) findViewById(R.id.percent_indicator);
                        percentText.setText(lightlyActivePercentage + "%");

                        TextView conNeedText = (TextView) findViewById(R.id.consumed_over_needed);
                        conNeedText.setText((int)Math.round(consumed) + "cal/" + (int)Math.round(lightlyActiveCalories) + "cal");

                        TextView consumedText = (TextView) findViewById(R.id.consumed_value);
                        consumedText.setText((int)Math.round(consumed) + " cal");

                        TextView neededText = (TextView) findViewById(R.id.needed_value);
                        neededText.setText((int)Math.round(lightlyActiveCalories) + " cal");

                        TextView leftoverText = (TextView) findViewById(R.id.leftover_value);
                        leftoverText.setText((int)Math.round(lightlyActiveCaloriesRemaining) + " cal");
                    }
                    else if (userStatus.equals("Moderately Active")) {
                        TextView printUserStatus = (TextView) findViewById(R.id.activity_type);
                        printUserStatus.setText(userStatus + " Target");

                        final FitChart fitChart = (FitChart)findViewById(R.id.fitChart);
                        fitChart.setValue(moderatelyActivePercentage);

                        TextView percentText = (TextView) findViewById(R.id.percent_indicator);
                        percentText.setText(moderatelyActivePercentage + "%");

                        TextView conNeedText = (TextView) findViewById(R.id.consumed_over_needed);
                        conNeedText.setText((int)Math.round(consumed) + "cal/" + (int)Math.round(moderatelyActiveCalories) + "cal");

                        TextView consumedText = (TextView) findViewById(R.id.consumed_value);
                        consumedText.setText((int)Math.round(consumed) + " cal");

                        TextView neededText = (TextView) findViewById(R.id.needed_value);
                        neededText.setText((int)Math.round(moderatelyActiveCalories) + " cal");

                        TextView leftoverText = (TextView) findViewById(R.id.leftover_value);
                        leftoverText.setText((int)Math.round(moderatelyActiveCaloriesRemaining) + " cal");
                    }
                    else if (userStatus.equals("Very Active")) {
                        TextView printUserStatus = (TextView) findViewById(R.id.activity_type);
                        printUserStatus.setText(userStatus + " Target");

                        final FitChart fitChart = (FitChart)findViewById(R.id.fitChart);
                        fitChart.setValue(veryActivePercentage);

                        TextView percentText = (TextView) findViewById(R.id.percent_indicator);
                        percentText.setText(veryActivePercentage + "%");

                        TextView conNeedText = (TextView) findViewById(R.id.consumed_over_needed);
                        conNeedText.setText((int)Math.round(consumed) + "cal/" + (int)Math.round(veryActiveCalories) + "cal");

                        TextView consumedText = (TextView) findViewById(R.id.consumed_value);
                        consumedText.setText((int)Math.round(consumed) + " cal");

                        TextView neededText = (TextView) findViewById(R.id.needed_value);
                        neededText.setText((int)Math.round(veryActiveCalories) + " cal");

                        TextView leftoverText = (TextView) findViewById(R.id.leftover_value);
                        leftoverText.setText((int)Math.round(veryActiveCaloriesRemaining) + " cal");
                    }
                    else if (userStatus.equals("Super Active")) {
                        TextView printUserStatus = (TextView) findViewById(R.id.activity_type);
                        printUserStatus.setText(userStatus + " Target");

                        final FitChart fitChart = (FitChart)findViewById(R.id.fitChart);
                        fitChart.setValue(superActivePercentage);

                        TextView percentText = (TextView) findViewById(R.id.percent_indicator);
                        percentText.setText(superActivePercentage + "%");

                        TextView conNeedText = (TextView) findViewById(R.id.consumed_over_needed);
                        conNeedText.setText((int)Math.round(consumed) + "cal/" + (int)Math.round(superActiveCalories) + "cal");

                        TextView consumedText = (TextView) findViewById(R.id.consumed_value);
                        consumedText.setText((int)Math.round(consumed) + " cal");

                        TextView neededText = (TextView) findViewById(R.id.needed_value);
                        neededText.setText((int)Math.round(superActiveCalories) + " cal");

                        TextView leftoverText = (TextView) findViewById(R.id.leftover_value);
                        leftoverText.setText((int)Math.round(superActiveCaloriesRemaining) + " cal");
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    @Override
    // This makes it so that if the users press the back button, it exits the app
    public void onBackPressed() {
        //if (isTaskRoot()) {
            Intent myIntent = new Intent(Intent.ACTION_MAIN);
            myIntent.addCategory(Intent.CATEGORY_HOME);
            startActivity(myIntent);
        //}
        /*
        else {
            Intent myIntent = getIntent();
            String callingActivity = myIntent.getStringExtra("Calling Activity");

            if (callingActivity != null && callingActivity.equals("LogInActivity")) {
                Intent myIntent2 = new Intent(Intent.ACTION_MAIN);
                myIntent2.addCategory(Intent.CATEGORY_HOME);
                myIntent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(myIntent2);
            }
            else
                finish();
        }

         */
    }
}

