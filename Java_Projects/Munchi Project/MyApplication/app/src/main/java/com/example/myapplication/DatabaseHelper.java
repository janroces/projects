package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

// This class is responsible for handling the database used by Munchi.
// Contains methods for adding/removing user info to/from the Munchi DB.
public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper dbInstance;

    private static final String DATABASE_NAME = "Munchi.db";
    private static final String TABLE_NAME = "User_Info";
    private static final String COL1 = "EMAIL_ADDRESS";
    private static final String COL2 = "PASSWORD";
    private static final String COL3 = "AGE";
    private static final String COL4 = "GENDER";
    private static final String COL5 = "HEIGHT_IN_FEET";
    private static final String COL6 = "HEIGHT_IN_INCHES";
    private static final String COL7 = "WEIGHT";

    // The Munchi DB is created when this constructor is called. An object of this class
    // can't be created using this constructor since its private, so we use the "synchronized" method below to get around that.
    private DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 3); // factory = null;  version = 2
    }

    // This method makes sure that ONLY 1 instance of this class is ever created. The idea is
    // that we don't want the entire application to keep creating multiple databases. Only 1 database is needed.
    public static synchronized DatabaseHelper getInstance(Context context) {
        if (dbInstance == null)
            dbInstance = new DatabaseHelper(context);
        return dbInstance;
    }

    @Override
    // Called the very first time the entire project is run. Often used for creating the tables in the db.
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " + COL1 + " TEXT, " + COL2 + " TEXT, " + COL3 + " INTEGER, "
                                                        + COL4 + " TEXT, " + COL5 + " INTEGER, " + COL6 + " INTEGER, " + COL7 + " INTEGER )");
    }

    @Override
    // This method is used to alter existing tables. Called when we want to upgrade/modify
    // the DB structure
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    // Inserts user's info into the database
    public long insertData(String usernameInput, String passwordInput, int age, String gender, int heightFeet, int heightInches, int weight) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL1, usernameInput);
        values.put(COL2, passwordInput);
        values.put(COL3, age);
        values.put(COL4, gender);
        values.put(COL5, heightFeet);
        values.put(COL6, heightInches);
        values.put(COL7, weight);

        long x = db.insert(TABLE_NAME, null, values);
        db.close(); // closes database connection. done to avoid leaks.

        return x;
    }

    // Deletes the entire table storing all the users' info
    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
    }

    // Checks if the username provided already exists in the DB
    public boolean checkIfUserExists(String usernameInputted) {
        SQLiteDatabase db = this.getReadableDatabase();

        String myQuery = "SELECT * from " + TABLE_NAME + " WHERE " + COL1 + " = ?";

        Cursor cursor = db.rawQuery(myQuery, new String[]{usernameInputted});

        if (cursor.getCount() <= 0) {
            cursor.close();
            db.close(); // closes database connection. done to avoid leaks.
            return false;
        }

        cursor.close();
        db.close(); // closes database connection. done to avoid leaks.

        return true;
    }

    // Retrieves the user's password from the DB
    /*
    public String retrievePassword(String usernameInputted) {
        SQLiteDatabase db = this.getReadableDatabase();
        String myQuery = "SELECT * from " + TABLE_NAME + " WHERE " + COL1 + " = ?";

        Cursor cursor = db.rawQuery(myQuery, new String[] {usernameInputted});

        cursor.moveToFirst();
        String storedPassword = cursor.getString(cursor.getColumnIndex(COL2));

        cursor.close();
        db.close(); // closes database connection. done to avoid leaks.

        return storedPassword;
    }
     */

    // Retrieves elements from the database
    public String printDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        String myQuery = "SELECT * FROM " + TABLE_NAME;

        String[] projection = {COL1, COL2, COL3, COL4, COL5, COL6, COL7}; // which columns from the row to display

        Cursor cursor = db.rawQuery(myQuery, null);//, projection, null, null, null, null, null);

        if (cursor.getCount() <= 0)
            return "";
        else {
            String userNameList = "";

            while(cursor.moveToNext()) {
                String username = cursor.getString(0);
                String password = cursor.getString(cursor.getColumnIndex(COL2));
                int age = cursor.getInt(cursor.getColumnIndex(COL3));
                String gender = cursor.getString(cursor.getColumnIndex(COL4));
                int heightFeet = cursor.getInt(cursor.getColumnIndex(COL5));
                int heightInches = cursor.getInt(cursor.getColumnIndex(COL6));
                int weight = cursor.getInt(cursor.getColumnIndex(COL7));

                userNameList += username + ", " + password + ", " + age + ", " + gender + ", " + heightFeet + ", " + heightInches + ", " + weight + "\n";
            }

            cursor.close();
            db.close(); // closes database connection. done to avoid leaks.

            return userNameList;
        }
    }
}
