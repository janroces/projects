package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LogInActivity extends AppCompatActivity {
    private EditText email, password;
    private Button backButton, loginButton, forgotPasswordBtn;

    private FirebaseAuth myFirebaseAuth;
    private DatabaseReference munchiDBref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        email = (EditText) findViewById(R.id.emailLogin_ET);
        password = (EditText) findViewById(R.id.passwordLogin_ET);
        backButton = (Button) findViewById(R.id.backBtn_LI_activity);
        loginButton = (Button) findViewById(R.id.confirmLoginBtn);
        forgotPasswordBtn = (Button) findViewById(R.id.forgotPassword_btn);

        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyCredentials();
            }
        });

        forgotPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });
    }

    public void verifyCredentials() {
        if (email.getText().toString().trim().equals("") || password.getText().toString().equals(""))
            Toast.makeText(getBaseContext(), "Please enter your email and/or password", Toast.LENGTH_LONG).show();
        else {
            myFirebaseAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        if (myFirebaseAuth.getCurrentUser().isEmailVerified())
                            userHomePage();
                        else
                            Toast.makeText(getBaseContext(),"Please verify your email address.", Toast.LENGTH_LONG).show();
                    }
                    else
                        Toast.makeText(getBaseContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void userHomePage() {
        Intent myIntent = new Intent(this, HomePage.class);
        myIntent.putExtra("Calling Activity", "LogInActivity");
        startActivity(myIntent);
    }

    private void forgotPassword() {
        Intent myIntent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(myIntent);
    }
}
