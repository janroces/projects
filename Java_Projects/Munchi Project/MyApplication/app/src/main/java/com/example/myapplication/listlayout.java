package com.example.myapplication;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class listlayout extends BaseAdapter {

    public static double calories = 0.0;
    public static double calo3;

    private List<ListItem> listItems;
    private Context context;

    FirebaseAuth myFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser munchiUser = myFirebaseAuth.getCurrentUser();

    private DatabaseReference munchiDBref;


    public listlayout(Context context, List<ListItem>listItems){
        //super(context, R.layout.listlayout);
        this.context = context;
        this.listItems = listItems;
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");
    }

    @Override
    public int getCount() {return listItems.size();}

    @Override
    public Object getItem(int position){return listItems.get(position);}

    @Override
    public long getItemId(int position){return listItems.indexOf(getItem(position));}

    private class ViewHolder{
        TextView tvw;
        ImageView ivw;
        TextView cvw;
        TextView svw;
        ViewHolder(View itemView){
            tvw = (TextView) itemView.findViewById(R.id.recipeId);
            ivw = (ImageView) itemView.findViewById(R.id.imageId);
            cvw = (TextView) itemView.findViewById(R.id.caloId);
            svw = (TextView) itemView.findViewById(R.id.servID);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View r = convertView;
        ViewHolder viewHolder = null;
        if(r == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            r = mInflater.inflate(R.layout.listlayout, null);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) r.getTag();
        }

        final ListItem row_pos = listItems.get(position);
        //viewHolder.ivw.setImageResource(row_pos.getImageUrl());
        viewHolder.tvw.setText(row_pos.getRecipeName());
        viewHolder.cvw.setText(row_pos.getCalo());
        viewHolder.svw.setText(row_pos.getServing());
        Picasso.with(context)
                .load(row_pos.getImageUrl())
                .into(viewHolder.ivw);


        Button addCalories = (Button)r.findViewById(R.id.addButton);
        Button lookRecipe = (Button)r.findViewById(R.id.recipeButton);

        lookRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ListItem product = listItems.get(position);
                i.setData(Uri.parse(product.getSourceURL()));
                context.startActivity(i);
            }
        });

        if (munchiUser != null) {
            addCalories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListItem product = listItems.get(position);
                    String calo = product.getCalo();
                    Log.d("calories of recipe", calo);
                    int len = calo.length();
                    int first = 0;
                    int last = len;
                    for (int i = 0; i < len; i++) {
                        char c = calo.charAt(i);
                        first = i;
                        if (c == ' ') {
                            last = i;
                            break;
                        }
                    }


                    String calo2 = calo.substring(0, last);
                    calo3 = Double.parseDouble(calo2);

                    Toast msg = Toast.makeText(context, "Adding " + calo2 + " calories.", Toast.LENGTH_LONG);


                    msg.show();

                    double c = Double.parseDouble(calo2);
                    addCalories(c);
                    saveInfo();


                    Log.d("dynamic calorie count ", String.valueOf(calories));
                }
            });
        }
        else {
            addCalories.setVisibility(Button.GONE);
        }

        return r;

    }

    private void saveInfo() {

        Query userQuery = munchiDBref.orderByChild("email").equalTo(munchiUser.getEmail().toString());
        userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    Users newUser = singleSnapshot.getValue(Users.class);

                    newUser.setCalories(calo3);
                    munchiDBref.child(newUser.getKey()).setValue(newUser);


                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void addCalories(double c) {
        calories += c;
    }
}
