package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivityLevel extends AppCompatActivity {
    private Button backButton, nextButton;
    private static RadioButton sedentary, lightlyActive, moderatelyActive, veryActive, superActive;
    public static RadioGroup radioGroup;
    private RadioButton genderRadioButton, activityRadioButton;

    private FirebaseAuth myFirebaseAuth;
    private FirebaseUser munchiUser;
    private DatabaseReference munchiDBref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_lvl);

        backButton = (Button) findViewById(R.id.back_button);
        nextButton = (Button) findViewById(R.id.next_button);
        sedentary = (RadioButton) findViewById(R.id.sedentary_radioButton);
        lightlyActive = (RadioButton) findViewById(R.id.lightlyActive_radioButton);
        moderatelyActive = (RadioButton) findViewById(R.id.moderatelyActive_radioButton);
        veryActive = (RadioButton) findViewById(R.id.veryActive_radioButton);
        superActive = (RadioButton) findViewById(R.id.superActive_radioButton);
        radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup2);


        int genderButtonId = EnterUserMetricsActivity.radioGroup.getCheckedRadioButtonId();
        genderRadioButton = (RadioButton) EnterUserMetricsActivity.radioGroup.findViewById(genderButtonId);

        //int activityLvlButtonId = ActivityLevel.radioGroup.getCheckedRadioButtonId();
        //activityRadioButton = (RadioButton) ActivityLevel.radioGroup.findViewById(activityLvlButtonId);



        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiUser = myFirebaseAuth.getCurrentUser();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // closes the current activity, which means that we return to the activity that called the current activity
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyInfo();
            }
        });
    }

    private void verifyInfo() {
        if (!sedentary.isChecked() && !lightlyActive.isChecked() && !moderatelyActive.isChecked() && !veryActive.isChecked() && !superActive.isChecked())
            Toast.makeText(getBaseContext(), "Please select an activity level.", Toast.LENGTH_LONG).show();
        else
            storeInfo();
    }

    private void storeInfo() {
        String userGender;

        if (sedentary.isChecked())
            userGender = "Sedentary";
        else if (lightlyActive.isChecked())
            userGender = "Lightly Active";
        else if (moderatelyActive.isChecked())
            userGender = "Moderately Active";
        else if (veryActive.isChecked())
            userGender = "Very Active";
        else
            userGender = "Super Active";



        String id = munchiDBref.push().getKey();

        Users newUser = new Users(munchiUser.getEmail(), Integer.parseInt(EnterUserMetricsActivity.age.getText().toString()),
                genderRadioButton.getText().toString(), Integer.parseInt(EnterUserMetricsActivity.heightFeet.getText().toString()),
                Integer.parseInt(EnterUserMetricsActivity.heightInches.getText().toString()), Integer.parseInt(EnterUserMetricsActivity.weight.getText().toString()),
                userGender, id, 0);

        munchiDBref.child(id).setValue(newUser);

        userHomePage();
    }

    private void userHomePage() {
        Intent myIntent = new Intent(this, HomePage.class);
        startActivity(myIntent);
    }
}
