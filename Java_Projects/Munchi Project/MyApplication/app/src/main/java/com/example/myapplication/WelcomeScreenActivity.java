package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomeScreenActivity extends AppCompatActivity {

    // Button declarations
    private Button skipButton;
    private Button loginButton;
    private Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        skipButton = (Button) findViewById(R.id.skipButton);
        loginButton = (Button) findViewById(R.id.loginBtn);
        signUpButton = (Button) findViewById(R.id.signUpBtn);

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPage();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpPage();
            }
        });
    }

    private void mainMenu() {
        Intent myIntent = new Intent(this, EnterIngredientsActivity.class);
        startActivity(myIntent);
    }

    private void loginPage() {
        Intent myIntent = new Intent(this, LogInActivity.class);
        startActivity(myIntent);
    }

    private void signUpPage() {
        Intent myIntent = new Intent(this, ContractPageActivity.class);
        startActivity(myIntent);
    }

    // Commented out, but doesn't really change anything??
    /*
    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(Intent.ACTION_MAIN);
        myIntent.addCategory(Intent.CATEGORY_HOME);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
    }
    */
}
