package com.example.myapplication;

public class Users {
    private String email;
    private int age;
    private String gender;
    private int heightInFeet;
    private int heightInInches;
    private int weight;
    private String activityLevel;
    private String key;
    private double calories;

    // Even though the default constructor is empty, we still need it because Firebase uses
    // is to construct a new instance of the class
    public Users() {
    }

    public Users(String email, int age, String gender, int heightInFeet, int heightInInches, int weight, String activityLevel, String key, double calories) {
        this.email = email;
        this.age = age;
        this.gender = gender;
        this.heightInFeet = heightInFeet;
        this.heightInInches = heightInInches;
        this.weight = weight;
        this.activityLevel = activityLevel;
        this.key = key;
        this.calories = calories;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getHeightInFeet() {
        return heightInFeet;
    }

    public void setHeightInFeet(int heightInFeet) {
        this.heightInFeet = heightInFeet;
    }

    public int getHeightInInches() {
        return heightInInches;
    }

    public void setHeightInInches(int heightInInches) {
        this.heightInInches = heightInInches;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(String activityLevel) {
        this.activityLevel = activityLevel;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories += calories;
    }

    public String getKey() {
        return key;
    }
}
