package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity {
    private FirebaseAuth myFirebaseAuth;
    private FirebaseUser munchiUser;
    private DatabaseReference munchiDBref;

    public static TextView userEmail;
    public static TextView userAge;
    public static TextView userGender;
    public static TextView userHeight;
    public static TextView userWeight;
    public static TextView userActivityLevel;

    private Button modifyInfoBtn;
    private Button signOut;

    private ImageButton homePageButton;
    private ImageButton recipeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiUser = myFirebaseAuth.getCurrentUser();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        userEmail = (TextView) findViewById(R.id.profile_emailTV);
        userAge = (TextView) findViewById(R.id.profile_ageTV);
        userGender = (TextView) findViewById(R.id.profile_genderTV);
        userHeight = (TextView) findViewById(R.id.profile_heightTV);
        userWeight = (TextView) findViewById(R.id.profile_weightTV);
        userActivityLevel = (TextView) findViewById(R.id.profile_activityLevel);
        modifyInfoBtn = (Button) findViewById(R.id.profile_modifyBTN);
        signOut = (Button) findViewById(R.id.profile_signOutBtn);

        homePageButton = (ImageButton) findViewById(R.id.home_button); // For Recipe Page
        recipeButton = (ImageButton) findViewById(R.id.recipe_button);

        displayUserInfo();

        modifyInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyUserInfo();
            }
        });

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifySignOut();
            }
        });

        homePageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                homePage();
            }
        });

        recipeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                recipePage();
            }
        });
    }

    private void displayUserInfo() {
        Query userQuery = munchiDBref.orderByChild("email").equalTo(munchiUser.getEmail().toString());
        userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    Users newUser = singleSnapshot.getValue(Users.class);

                    userEmail.setText("Email: " + newUser.getEmail());
                    userAge.setText("Age: " + newUser.getAge());
                    userGender.setText("Gender: " + newUser.getGender());
                    userHeight.setText("Height: " + newUser.getHeightInFeet() + "ft " + newUser.getHeightInInches() + "in");
                    userWeight.setText("Weight: " + newUser.getWeight() + " lbs");
                    userActivityLevel.setText("Activity Level: " + newUser.getActivityLevel());
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void modifyUserInfo() {
        Intent myIntent = new Intent(this, ModifyUserInfoActivity.class);
        startActivity(myIntent);
    }

    private void verifySignOut() {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(this);
        myDialog.setTitle("Sign Out");
        myDialog.setMessage("Are you sure you want to sign out of your account?");

        myDialog.setPositiveButton("SIGN OUT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseAuth.getInstance().signOut();

                Intent myIntent = new Intent(getBaseContext(), WelcomeScreenActivity.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
            }
        });

        myDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog myAlertDialog = myDialog.create();
        myAlertDialog.show();
    }





    private void homePage() {
        Intent intent = new Intent(this, HomePage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void recipePage() {
        Intent intent = new Intent(this, EnterIngredientsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(this, HomePage.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
    }
}
