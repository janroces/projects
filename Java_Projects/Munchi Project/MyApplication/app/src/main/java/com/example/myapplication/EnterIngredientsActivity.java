package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class EnterIngredientsActivity extends AppCompatActivity {
    //Initialization
    //----------------------------------------------
    private EditText txt;
    private Button save;
    private Button clear;
    private Button find;
    private ListView show;
    private static ArrayList<String> ingredientArr = new ArrayList<String>();
    private ArrayAdapter<String> adapterForIngredients;
    private ArrayAdapter<String> adapterForAutoFill;

    private ImageButton homeButton;
    private ImageButton profileButton;
    private AutoCompleteTextView myACTV;

    FirebaseAuth myFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser munchiUser = myFirebaseAuth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_ingredients);


        save = (Button) findViewById(R.id.enterButton);
        clear = (Button) findViewById(R.id.clearButton);
        find = (Button) findViewById(R.id.findRecipes);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveIngredient();
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearIngredients();
            }
        });
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchGetRecipes();
            }
        });


        String[] ingredients = getResources().getStringArray(R.array.INGREDIENTS);
        adapterForAutoFill = new ArrayAdapter<String>(EnterIngredientsActivity.this, android.R.layout.simple_list_item_1, ingredients);

        myACTV = findViewById(R.id.ingredientText);
        adapterForIngredients = new ArrayAdapter<>(EnterIngredientsActivity.this, android.R.layout.simple_list_item_1, ingredientArr);
        myACTV.setAdapter(adapterForAutoFill);

        show = (ListView) findViewById(R.id.ingredientList);

        if (munchiUser != null) // the user is currently logged in
            show.setAdapter(adapterForIngredients);
        else
            ingredientArr.clear();


        homeButton = (ImageButton) findViewById(R.id.home_button);
        profileButton = (ImageButton) findViewById(R.id.profile_button);

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePage();
            }
        });
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilePage();
            }
        });


        hideKeyboard();
    }

    private void saveIngredient() {
        String getIngredient = myACTV.getText().toString();

        if(getIngredient == null || getIngredient.trim().equals("")){
            Toast.makeText(getBaseContext(), "Can't input empty ingredient", Toast.LENGTH_LONG).show();
        }
        else{
            ingredientArr.add(0, getIngredient);
            ArrayAdapter<String> adapterForIngredients = new ArrayAdapter<String>(EnterIngredientsActivity.this, android.R.layout.simple_list_item_1, ingredientArr);
            show.setAdapter(adapterForIngredients);
            ((EditText)findViewById(R.id.ingredientText)).setText("");

            hideKeyboard();
        }
    }

    private void clearIngredients() {
        if(ingredientArr.size() == 0){
            Toast.makeText(EnterIngredientsActivity.this, "Ingredient List is already empty", Toast.LENGTH_LONG).show();
        }
        else {
            ingredientArr.clear();
            Toast.makeText(EnterIngredientsActivity.this, "Ingredient List has now been cleared", Toast.LENGTH_LONG).show();

            ArrayAdapter<String> adapterForIngredients = new ArrayAdapter<String>(EnterIngredientsActivity.this, android.R.layout.simple_list_item_1, ingredientArr);
            show.setAdapter(adapterForIngredients);
            ((EditText)findViewById(R.id.ingredientText)).setText("");
        }

    }

    //Find recipes with API GET request
    //-------------------------------------------------------------------------------
    private void launchGetRecipes(){
        find = (Button)findViewById(R.id.findRecipes);
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check if ingredient array is empty
                if(ingredientArr.size() == 0){
                    Toast.makeText(EnterIngredientsActivity.this, "Need at least one ingredient!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(EnterIngredientsActivity.this, "Finding Recipes...", Toast.LENGTH_SHORT).show();


                    //Share ingredientArr to GetRecipes activity
                    //-------------------------------------------------------------------------------
                    Intent recipeIntent = new Intent(EnterIngredientsActivity.this, GetRecipes.class);  //create intent of GetRecipes
                    recipeIntent.putExtra("ingredientArr", ingredientArr);

                    //Launch find recipe activity
                    //-------------------------------------------------------------------------------
                    startActivity(recipeIntent);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hideKeyboard() {
        View myView = this.getCurrentFocus();

        if (myView != null) {
            InputMethodManager myIMM = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            myIMM.hideSoftInputFromWindow(myView.getWindowToken(), 0);
        }
    }

    private void homePage() {
        // if the user is currently logged in
        if (munchiUser != null) {
            Intent intent = new Intent(this, HomePage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else { // if the user is NOT logged in
            Intent intent = new Intent(this, LogInOrSignUpActivity.class);
            startActivity(intent);
        }
    }

    private void profilePage() {
        // if the user is currently logged in
        if (munchiUser != null) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else { // if the user is NOT logged in
            Intent intent = new Intent(this, LogInOrSignUpActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        // if the user is currently logged in
        if (munchiUser != null) {
            Intent myIntent = new Intent(this, HomePage.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(myIntent);
        }
        else { // if the user is NOT logged in
            // returns the user to the welcome screen
            ingredientArr.clear();
            finish();
        }
    }
}
