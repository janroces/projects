// DO NOT TOUCH PETE (or anyone)


package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;

public class SettingsActivity extends AppCompatActivity {
    private TextView userEmail, userInfoTV;
    private Button signOut, deleteAccountBtn, showUserInfoBtn;

    private FirebaseAuth myFirebaseAuth;
    private FirebaseUser munchiUser;
    private DatabaseReference munchiDBref;

    private TextView caloriesConsumed;

    private TextView sedentaryResult;
    private TextView lightlyActiveResult;
    private TextView moderatelyActiveResult;
    private TextView veryActiveResult;
    private TextView superActiveResult;

    private TextView sedentaryCaloriesRemainingResult;
    private TextView lightlyActiveCaloriesRemainingResult;
    private TextView moderatelyActiveCaloriesRemainingResult;
    private TextView veryActiveCaloriesRemainingResult;
    private TextView superActiveCaloriesRemainingResult;

    private TextView sedentaryPercentageResult;
    private TextView lightlyActivePercentageResult;
    private TextView moderatelyActivePercentageResult;
    private TextView veryActivePercentageResult;
    private TextView superActivePercentageResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        userEmail = (TextView) findViewById(R.id.userEmail_tv);
        signOut = (Button) findViewById(R.id.signOut_btn);
        deleteAccountBtn = (Button) findViewById(R.id.deleteAccount_btn);
        showUserInfoBtn = (Button) findViewById(R.id.userInfo_btn);
        userInfoTV = (TextView) findViewById(R.id.userInfo_textView);

        // added

        caloriesConsumed = (TextView) findViewById(R.id.calories_consumed_result);

        sedentaryResult = (TextView) findViewById(R.id.sedentary_result);
        sedentaryCaloriesRemainingResult = (TextView) findViewById(R.id.sedentary_calories_remaining);
        sedentaryPercentageResult = (TextView) findViewById(R.id.sedentary_percentage);

        lightlyActiveResult = (TextView) findViewById(R.id.lightly_active_result);
        lightlyActiveCaloriesRemainingResult = (TextView) findViewById(R.id.lightly_active_calories_remaining);
        lightlyActivePercentageResult = (TextView) findViewById(R.id.lightly_active_percentage);

        moderatelyActiveResult = (TextView) findViewById(R.id.moderately_active_result);
        moderatelyActiveCaloriesRemainingResult = (TextView) findViewById(R.id.moderately_active_calories_remaining);
        moderatelyActivePercentageResult = (TextView) findViewById(R.id.moderately_active_percentage);

        veryActiveResult = (TextView) findViewById(R.id.very_active_result);
        veryActiveCaloriesRemainingResult = (TextView) findViewById(R.id.very_active_calories_remaining);
        veryActivePercentageResult = (TextView) findViewById(R.id.very_active_percentage);

        superActiveResult = (TextView) findViewById(R.id.super_active_result);
        superActiveCaloriesRemainingResult = (TextView) findViewById(R.id.super_active_calories_remaining);
        superActivePercentageResult = (TextView) findViewById(R.id.super_active_percentage);
        //

        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiUser = myFirebaseAuth.getCurrentUser();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        userEmail.setText(munchiUser.getEmail());


        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUserOut();
            }
        });

        deleteAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAccount();
            }
        });

        showUserInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUserInfo();
            }
        });
    }

    private void signUserOut() {
        FirebaseAuth.getInstance().signOut();
        Intent myIntent = new Intent(SettingsActivity.this, WelcomeScreenActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(myIntent);
    }

    private void deleteAccount() {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(SettingsActivity.this);
        myDialog.setTitle("Delete account");
        myDialog.setMessage("Are you sure you want to delete your account?");

        myDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                munchiUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            // The 1st block of code below removes the node in the DB that contains the current user's information
                            Query myQuery = munchiDBref.orderByChild("email").equalTo(userEmail.getText().toString());

                            myQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot i: dataSnapshot.getChildren()) {
                                        i.getRef().removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });


                            Toast.makeText(SettingsActivity.this, "Your Munchi account has been deleted.", Toast.LENGTH_LONG).show();

                            Intent myIntent = new Intent(SettingsActivity.this, WelcomeScreenActivity.class);
                            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(myIntent);
                        }
                        else
                            Toast.makeText(SettingsActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog myAlertDialog = myDialog.create();
        myAlertDialog.show();
    }


    private void showUserInfo() {


        Query userQuery = munchiDBref.orderByChild("email").equalTo(userEmail.getText().toString());
        userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    Users newUser = singleSnapshot.getValue(Users.class);
                    userInfoTV.setText(newUser.getEmail() + "\n" + newUser.getAge() + "\n" + newUser.getGender() + "\n" +
                            newUser.getHeightInFeet() + "\n" + newUser.getHeightInInches() + "\n" +
                            newUser.getWeight() + "\n" + newUser.getActivityLevel());

                    //added
                    double consumed = 0.0;
                    double bmr = 0.0;
                    double sedentaryCalories = 0.0;
                    double lightlyActiveCalories = 0.0;
                    double moderatelyActiveCalories = 0.0;
                    double veryActiveCalories = 0.0;
                    double superActiveCalories = 0.0;
                    double sedentaryCaloriesRemaining = 0.0;
                    double lightlyActiveCaloriesRemaining = 0.0;
                    double moderatelyActiveCaloriesRemaining = 0.0;
                    double veryActiveCaloriesRemaining = 0.0;
                    double superActiveCaloriesRemaining = 0.0;
                    double sedentaryPercentage = 0.0;
                    double lightlyActivePercentage = 0.0;
                    double moderatelyActivePercentage = 0.0;
                    double veryActivePercentage = 0.0;
                    double superActivePercentage = 0.0;

                    String gender = newUser.getGender();
                    double weightInPounds = newUser.getWeight();
                    double age = newUser.getAge();
                    double heightFt = newUser.getHeightInFeet();
                    double heightIn = newUser.getHeightInInches();

                    double weight = weightInPounds * 0.45359237;

                    double height;
                    heightFt = heightFt * 30.48;
                    heightIn = heightIn * 2.54;
                    height = heightFt + heightIn;

                    if (gender.equals("Male"))
                        bmr = 10 * weight + 6.25 * height - 5 * age + 5;
                    else if (gender.equals("Female"))
                        bmr = 10 * weight + 6.25 * height - 5 * age - 161;

                    sedentaryCalories = bmr * 1.2;
                    lightlyActiveCalories = bmr * 1.375;
                    moderatelyActiveCalories = bmr * 1.55;
                    veryActiveCalories = bmr * 1.725;
                    superActiveCalories = bmr * 1.9;

                    sedentaryCaloriesRemaining = sedentaryCalories - consumed;
                    lightlyActiveCaloriesRemaining = lightlyActiveCalories - consumed;
                    moderatelyActiveCaloriesRemaining = moderatelyActiveCalories - consumed;
                    veryActiveCaloriesRemaining = veryActiveCalories - consumed;
                    superActiveCaloriesRemaining = superActiveCalories - consumed;

                    sedentaryPercentage = (consumed / sedentaryCalories) * 100;
                    lightlyActivePercentage = (consumed / lightlyActiveCalories) * 100;
                    moderatelyActivePercentage = (consumed / moderatelyActiveCalories) * 100;
                    veryActivePercentage = (consumed / veryActiveCalories) * 100;
                    superActivePercentage = (consumed / superActiveCalories) * 100;


                    sedentaryResult.setText("Sedentary target: " + sedentaryCalories);
                    sedentaryCaloriesRemainingResult.setText("Sedentary calories remaining: " + sedentaryCaloriesRemaining);
                    sedentaryPercentageResult.setText("Sedentary percentage: " + sedentaryPercentage + "%");

                    lightlyActiveResult.setText("Lightly active target: " + lightlyActiveCalories);
                    lightlyActiveCaloriesRemainingResult.setText("Lightly active calories remaining: " + lightlyActiveCaloriesRemaining);
                    lightlyActivePercentageResult.setText("Lightly active percentage: " + lightlyActivePercentage + "%");

                    moderatelyActiveResult.setText("Moderately active target: " +  moderatelyActiveCalories);
                    moderatelyActiveCaloriesRemainingResult.setText("Moderately active calories remaining: " + moderatelyActiveCaloriesRemaining);
                    moderatelyActivePercentageResult.setText("Moderately active percentage: " + moderatelyActivePercentage + "%");

                    veryActiveResult.setText("Very active target: " + veryActiveCalories);
                    veryActiveCaloriesRemainingResult.setText("Very active calories remaining: " + veryActiveCaloriesRemaining);
                    veryActivePercentageResult.setText("Very active percentage: " + veryActivePercentage + "%");

                    superActiveResult.setText("Super active target: " + superActiveCalories);
                    superActiveCaloriesRemainingResult.setText("Super active calories remaining: " + superActiveCaloriesRemaining);
                    superActivePercentageResult.setText("Super active percentage: " + superActivePercentage + "%");

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
     //   DataSnapshot dataSnapshot;
     //   DataSnapshot singleSnapshot : dataSnapshot.getChildren();
     //   Users munchiUser = singleSnapshot.getValue(Users.class);

    }


    @Override
    // If the user is logged into their account and they press the back button on their phone,
    // the entire application will be closed.
    public void onBackPressed() {
        Intent myIntent = new Intent(Intent.ACTION_MAIN);
        myIntent.addCategory(Intent.CATEGORY_HOME);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
    }
}
