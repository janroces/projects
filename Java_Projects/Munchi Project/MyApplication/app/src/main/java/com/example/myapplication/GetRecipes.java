package com.example.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GetRecipes extends AppCompatActivity {

    //Initialize
    //-------------------------------------------------------------------------------
    List<ListItem> listItems;
    //Recipe Ingredient endpoint
    String recipeURL = "https://api.spoonacular.com/recipes/findByIngredients?apiKey=fd6c2dc91e374c358e1d720f47a69f66&number=";

    //API authentication
    String apiAuth = "apiKey=fd6c2dc91e374c358e1d720f47a69f66";

    //API nutrition info
    String nutri = "/nutritionWidget.json?";

    //API source info
    String source = "/information?includeNutrition=false&";

    //default count of recipes to display
    int recipeCount = 7;

    //testing messages
    String calorie;
    String gResponse = "Good";
    String bResponse = "Bad";
    String empty = "empty";
    String test = "test";
    String dash = "-----------------------------------------";
    String one = "one";
    String two = "two";
    String three = "three";
    String wait = "Thread Waiting...";

    //iterator for display
    int k = 0;
    int counter = 0;

    // The Dock Bar Buttons private variables
    private ImageButton homeButton; // Declaration of home button
    private ImageButton recipesButton; // Declaration of recipes button
    private ImageButton profileButton; // Declaration of profile button

    public class recipeINFO {

        public ArrayList<String> titleArr = new ArrayList<String>();
        public ArrayList<String> imageArr = new ArrayList<String>();
        public ArrayList<String> recipeIDarr = new ArrayList<String>();
        public ArrayList<String> sourceURLArr = new ArrayList<String>();
        public ArrayList<String> calorieArr = new ArrayList<String>();
        public ArrayList<String> servingArr = new ArrayList<String>();

        //constructor
        public recipeINFO(ArrayList<String> title, ArrayList<String> image, ArrayList<String> id, ArrayList<String> url, ArrayList<String> cal, ArrayList<String> serv) {
            this.titleArr = title;
            this.imageArr = image;
            this.recipeIDarr = id;
            this.sourceURLArr = url;
            this.calorieArr = cal;
            this.servingArr = serv;
        }

        //data adding
        public void addTitle(String s) {titleArr.add(s);}
        public void addImage(String s) {imageArr.add(s);}
        public void addID(String s) {recipeIDarr.add(s);}
        public void addSource(String s) {sourceURLArr.add(s);}
        public void addCalories(String s) {calorieArr.add(s);}
        public void addServing(String s) {servingArr.add(s);}

        //data retrieving
        public String getTitle(int i) {return titleArr.get(i);}
        public String getImage(int i) {return imageArr.get(i);}
        public String getID(int i) {return recipeIDarr.get(i);}
        public String getSource(int i) {return sourceURLArr.get(i);}
        public String getCal(int i) {return calorieArr.get(i);}
        public String getServ(int i) {return servingArr.get(i);}

    }

    ArrayList<String> title = new ArrayList<String>();
    ArrayList<String> image = new ArrayList<String>();
    ArrayList<String> rid = new ArrayList<String>();
    ArrayList<String> surl = new ArrayList<String>();
    ArrayList<String> cal = new ArrayList<String>();
    ArrayList<String> serv = new ArrayList<String>();

    recipeINFO rInfo = new recipeINFO(title, image, rid, surl, cal, serv);
    //RequestQueue requestQueue = Volley.newRequestQueue(this);
    FirebaseAuth myFirebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser munchiUser = myFirebaseAuth.getCurrentUser();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_recipes);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        //Other initialization
        //-------------------------------------------------------------------------------
        listItems = new ArrayList<>();

        //get ingredient list from MainActivity
        ArrayList<String> ingredientArr = (ArrayList<String>) getIntent().getSerializableExtra("ingredientArr");

        if (munchiUser == null) // the user is not logged in
            recipeCount = 3;

        //Append headers to endpoint
        recipeURL += recipeCount;
        recipeURL += "&ingredients=";

        //Append ingredient list to endpoint
        //This creates a new endpoint URL to get the specific recipes
        for (int i = 0; i < ingredientArr.size(); i++) {
            if (i == 0)
                recipeURL += ingredientArr.get(i);
            else
                recipeURL += ",+" + ingredientArr.get(i);

            //console log recipeURL for checking
            Log.d("newURL " + i, recipeURL);
        }

        //Main function for API call
        getRecipes();

        // -----
        // Pete's Additions
        // The Dock Bar Button Triggers

        // Find by Id
        homeButton = (ImageButton) findViewById(R.id.home_button); // For Home Page
        recipesButton = (ImageButton) findViewById(R.id.recipe_button); // For Recipe Page
        profileButton = (ImageButton) findViewById(R.id.profile_button); // For Profile Page

        homeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                HomePage();
            }
        });

        recipesButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                recipesPage();
            }
        });

        profileButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                profilePage();
            }
        });
    }

    private void HomePage() {
        // if the user is currently logged in
        if (munchiUser != null) {
            Intent intent = new Intent(this, HomePage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else { // if the user is NOT logged in
            Intent intent = new Intent(this, LogInOrSignUpActivity.class);
            startActivity(intent);
        }
    }

    private void recipesPage() {
        finish();
    }

    private void profilePage() {
        // if the user is currently logged in
        if (munchiUser != null) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else { // if the user is NOT logged in
            Intent intent = new Intent(this, LogInOrSignUpActivity.class);
            startActivity(intent);
        }
    }

    //Recipe GET Request
    //-------------------------------------------------------------------------------
    public void getRecipes() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                recipeURL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {

                            for (int j=0; j<recipeCount; j++){
                                JSONObject o = response.getJSONObject(j);
                                String rTitle = o.getString("title");
                                String rImage = o.getString("image");
                                String rID = o.getString("id");

                                //add to recipeTitleArr
                                rInfo.addTitle(rTitle);

                                //add image url to imageArr
                                rInfo.addImage(rImage);

                                //add recipe id to recipeIDarr
                                rInfo.addID(rID);
                            }

                            //Volley dependency is asynchronous
                            //Having threads sleep at specific moments is necessary for variable population and retrieval
                            try{
                                Log.d("Status ", wait);
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            for (int j=0; j<recipeCount; j++) {
                                getCalories(j);
                                try{
                                    Log.d("Status ", wait);
                                    Thread.sleep(650);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                getSourceURL(j);
                            }

                        } catch (JSONException e) {
                            Log.d("Response ", bResponse);
                            e.printStackTrace();
                            Toast.makeText(GetRecipes.this, "No Recipes Found!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Rest Response Error", error.toString());
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

    //Get calorie information and display contents
    //-------------------------------------------------------------------------------
    public void getCalories(int n) {

        String id = rInfo.getID(n);
        String recipeCalURL = "https://api.spoonacular.com/recipes/";
        recipeCalURL += id;
        recipeCalURL += nutri;
        recipeCalURL += apiAuth;

        Log.d("calorie url " + n, recipeCalURL);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                recipeCalURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String rCalories = response.getString("calories");

                            rInfo.addCalories(rCalories);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonObjectRequest);

    }

    //Fetch source URL and serving size of recipe and append to array
    //-------------------------------------------------------------------------------
    public void getSourceURL(int m) {

        String id = rInfo.getID(m);
        String recipeInfoURL = "https://api.spoonacular.com/recipes/";
        recipeInfoURL += id;
        recipeInfoURL += source;
        recipeInfoURL += apiAuth;


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                recipeInfoURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String rSource = response.getString("sourceUrl");
                            int iServing = response.getInt("servings");
                            String rServing = String.valueOf(iServing);

                            //add serving size to servingArr
                            rInfo.addServing(rServing);

                            //add source url to sourceURLArr
                            rInfo.addSource(rSource);

                            String cal;
                            double dCal = Double.parseDouble(rInfo.getCal(k));
                            double dServ = Double.parseDouble(rInfo.getServ(k));
                            dCal = dCal / dServ;
                            cal = String.format("%.1f", dCal);

                            //creates ListItem object and displays
                            ListItem product = new ListItem(rInfo.getTitle(k), rInfo.getImage(k), cal + " Calories", rInfo.getSource(k), "per Serving");
                            k++;

                            listItems.add(product);
                            listlayout adapter = new listlayout(getApplicationContext(), listItems);
                            ListView show = (ListView) findViewById(R.id.recipeList);
                            show.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void listProduct(int n) {

        //String cal = rInfo.calPerServ(k);
        String cal;
        String src;
        double dCal = Double.parseDouble(rInfo.getCal(n));
        double dServ = Double.parseDouble(rInfo.getServ(n));

        Log.d("dCal ", String.valueOf(dCal));
        Log.d("dServ ", String.valueOf(dServ));

        dCal = dCal / dServ;
        //dCal = (dCal * 10) / 10.0;

        if (k==0) {
            src = rInfo.getSource(1);
        } else if (k==1) {
            src = rInfo.getSource(0);
        } else {
            src = rInfo.getSource(k);
        }

        cal = String.format("%.1f", dCal);

        ListItem product = new ListItem(rInfo.getTitle(n), rInfo.getImage(n), cal + " Calories", rInfo.getSource(n), "per Serving");
       // ListItem product = new ListItem(rInfo.getTitle(k), rInfo.getImage(k), cal + " Calories", rInfo.getSource(k), "per Serving");
        //ListItem product = new ListItem(titleArr.get(k), imageArr.get(k), "Calories: " + calorieArr.get(k), sourceURLArr.get(k)," Servings: " + servingArr.get(k).toString());
        listItems.add(product);

        listlayout adapter = new listlayout(getApplicationContext(), listItems);

        ListView show = (ListView) findViewById(R.id.recipeList);
        show.setAdapter(adapter);
        checkAPI(n);
        k++;
    }

    //Display API request to console
    //-------------------------------------------------------------------------------
    public void checkAPI(int i) {
        String num = String.valueOf(recipeCount);
        Log.d("API CHECK ", dash);
        Log.d("recipeTitleArr " + i, rInfo.getTitle(i));
        Log.d("imageArr " + i, rInfo.getImage(i));
        Log.d("recipeID " + i, rInfo.getID(i));
        Log.d("calories " + i, rInfo.getCal(i));
        Log.d("sourceURL " + i, rInfo.getSource(i));
        Log.d("servingArr " + i, rInfo.getServ(i));

    }

    //Click function to redirect to source url of recipe
    //(this code segment allows for clicking on list item to redirect to a website)
    //-------------------------------------------------------------------------------
    /*
    private void addClickListener() {
        ListView items = (ListView) findViewById(R.id.recipeList);
        items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                ListItem product = listItems.get(position);
                i.setData(Uri.parse(product.getSourceURL()));
                startActivity(i);
            }
        });
    }
    */

}