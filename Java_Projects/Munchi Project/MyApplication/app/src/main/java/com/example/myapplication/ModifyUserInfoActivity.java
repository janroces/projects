package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ModifyUserInfoActivity extends AppCompatActivity {
    private FirebaseAuth myFirebaseAuth;
    private FirebaseUser munchiUser;
    private DatabaseReference munchiDBref;

    private EditText age;
    private EditText heightInFeet;
    private EditText heightInInches;
    private EditText weight;

    public static RadioButton male, female, sedentaryMUI, lightlyActiveMUI, moderatelyActiveMUI, veryActiveMUI, superActiveMUI;
    public static RadioGroup radioGroup, radioGroup2;

    private Button back;
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_user_info);

        myFirebaseAuth = FirebaseAuth.getInstance();
        munchiUser = myFirebaseAuth.getCurrentUser();
        munchiDBref = FirebaseDatabase.getInstance().getReference("Users");

        //email = (EditText) findViewById(R.id.modifyEmail);
        age = (EditText) findViewById(R.id.modifyAge);
        heightInFeet = (EditText) findViewById(R.id.modifyHeightInFeet);
        heightInInches = (EditText) findViewById(R.id.modifyHeightInInches);
        weight = (EditText) findViewById(R.id.modifyWeight);

        male = (RadioButton) findViewById(R.id.male_radioButton_MUI);
        female = (RadioButton) findViewById(R.id.female_radioButton_MUI);
        radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup_MUI);

        sedentaryMUI = (RadioButton) findViewById(R.id.sedentary_radioButton_MUI);
        lightlyActiveMUI = (RadioButton) findViewById(R.id.L_Act_radioButton_MUI);
        moderatelyActiveMUI = (RadioButton) findViewById(R.id.M_Act_radioButton_MUI);
        veryActiveMUI = (RadioButton) findViewById(R.id.V_Act_radioButton_MUI);
        superActiveMUI = (RadioButton) findViewById(R.id.S_Act_radioButton_MUI);
        radioGroup2 = (RadioGroup) findViewById(R.id.myRadioGroup2_MUI);


        back = (Button) findViewById(R.id.backBtn_modifyUsersInfo);
        save = (Button) findViewById(R.id.saveModificationsBtn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInfo();
                finish();
            }
        });
    }

    private void saveInfo() {

        Query userQuery = munchiDBref.orderByChild("email").equalTo(munchiUser.getEmail().toString());
        userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    Users newUser = singleSnapshot.getValue(Users.class);

                    boolean somethingWasModified = false;

                    if (!age.getText().toString().equals("")) {
                        newUser.setAge(Integer.parseInt(age.getText().toString()));
                        somethingWasModified = true;
                    }

                    if (male.isChecked()) {
                        newUser.setGender("Male");
                        somethingWasModified = true;
                    }
                    else if (female.isChecked()) {
                        newUser.setGender("Female");
                        somethingWasModified = true;
                    }

                    if (!heightInFeet.getText().toString().equals("")) {
                        newUser.setHeightInFeet(Integer.parseInt(heightInFeet.getText().toString()));
                        somethingWasModified = true;
                    }

                    if (!heightInInches.getText().toString().equals("")) {
                        newUser.setHeightInInches(Integer.parseInt(heightInInches.getText().toString()));
                        somethingWasModified = true;
                    }

                    if (!weight.getText().toString().equals("")) {
                        newUser.setWeight(Integer.parseInt(weight.getText().toString()));
                        somethingWasModified = true;
                    }


                    if (sedentaryMUI.isChecked()) {
                        newUser.setActivityLevel("Sedentary");
                        somethingWasModified = true;
                    }
                    else if (lightlyActiveMUI.isChecked()) {
                        newUser.setActivityLevel("Lightly Active");
                        somethingWasModified = true;
                    }
                    else if (moderatelyActiveMUI.isChecked()) {
                        newUser.setActivityLevel("Moderately Active");
                        somethingWasModified = true;
                    }
                    else if (veryActiveMUI.isChecked()) {
                        newUser.setActivityLevel("Very Active");
                        somethingWasModified = true;
                    }
                    else if (superActiveMUI.isChecked()) {
                        newUser.setActivityLevel("Super Active");
                        somethingWasModified = true;
                    }


                    if (somethingWasModified) {
                        munchiDBref.child(newUser.getKey()).setValue(newUser);
                        showUpdatedInfo();
                    }
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    private void showUpdatedInfo() {
        Query userQuery = munchiDBref.orderByChild("email").equalTo(munchiUser.getEmail().toString());
        userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    Users newUser = singleSnapshot.getValue(Users.class);

                    ProfileActivity.userEmail.setText("Email: " + newUser.getEmail());
                    ProfileActivity.userAge.setText("Age: " + newUser.getAge());
                    ProfileActivity.userGender.setText("Gender: " + newUser.getGender());
                    ProfileActivity.userHeight.setText("Height: " + newUser.getHeightInFeet() + "ft " + newUser.getHeightInInches() + "in");
                    ProfileActivity.userWeight.setText("Weight: " + newUser.getWeight() + " lbs");
                    ProfileActivity.userActivityLevel.setText("Activity Level: " + newUser.getActivityLevel());
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
