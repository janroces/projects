package com.example.myapplication;

public class ListItem {
    private String recipeName;
    private String imageUrl;
    private String calo;
    private String sourceURL;
    private String serving;

    //List item Constructor
    public ListItem(String recipeName, String imageUrl, String calo, String sourceURL, String serving){
        this.recipeName =recipeName;
        this.imageUrl = imageUrl;
        this.calo = calo;
        this.sourceURL = sourceURL;
        this.serving = serving;


    }

    public String getRecipeName() {return recipeName;}

    public String getImageUrl() {return imageUrl;}

    public String getCalo() {return calo;}

    public String getSourceURL () {return sourceURL;}

    public String getServing () {return serving;}

}
